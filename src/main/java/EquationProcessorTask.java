import java.util.Scanner;

public class EquationProcessorTask {
    static Scanner sc = new Scanner(System.in);

    public static void main(String... args) {
        EquationProcessor equationProcessor = new EquationProcessor();
        System.out.print("Input your equation: ");
        String eq = sc.nextLine();
        double res = equationProcessor.solve(eq);
        System.out.println(res);
    }
}
