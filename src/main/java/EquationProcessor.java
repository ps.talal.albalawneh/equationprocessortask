import java.util.*;

public class EquationProcessor {

    Stack<Double> operandStack = new Stack<>();
    Stack<Character> operationStack = new Stack<>();

    public double solve(String equation) {
        equation = removeSpaces(equation);
        equation = lookForVariablesAndGetValues(equation);
        checkParenthesesBalance(equation);
        for (int i = 0; i < equation.length(); i++) {
            if (Character.isDigit(equation.charAt(i))) {
                int operand = getOperand(equation, i);
                i += Integer.toString(operand).length() - 1;
                operandStack.push((double) operand);
            } else if (isOperator(equation.charAt(i))) {
                char operation = getOperation(equation, i);
                if (operationStack.size() >= 1) {
                    checkPriority(operandStack, operationStack, operation, equation, i);
                } else {
                    operationStack.push(operation);
                }
            } else if (equation.charAt(i) == '(') {

                double[] res = solveSubEquation(equation, i + 1);
                i = (int) res[0];
                operandStack.push(res[1]);
            }
        }
        while (operationStack.size() > 0) {
            if (operandStack.size() < 2) {
                throw new IllegalArgumentException("something wrong with equation " + equation + " try rewrite it again.");
            }
            double second = operandStack.pop();
            double first = operandStack.pop();
            char op = operationStack.pop();
            applyOperation(operandStack, first, op, second);
        }
        return operandStack.peek();
    }

    // in-case the equation contains parentheses, this method will invoke recursively
    private double[] solveSubEquation(String eq, int idx) {
        Stack<Double> operandSubStack = new Stack<>();
        Stack<Character> operationSubStack = new Stack<>();
        while (eq.charAt(idx) != ')') {
            if (Character.isDigit(eq.charAt(idx))) {
                int operand = getOperand(eq, idx);
                idx += Integer.toString(operand).length() - 1;
                operandSubStack.push((double) operand);
            } else if (isOperator(eq.charAt(idx))) {
                char operation = getOperation(eq, idx);
                if (operationSubStack.size() >= 1) {
                    checkPriority(operandSubStack, operationSubStack, operation, eq, idx + 1);
                } else {
                    operationSubStack.push(operation);
                }
            } else if (eq.charAt(idx) == '(') {
                double[] res = solveSubEquation(eq, idx + 1);
                operandSubStack.push(Double.parseDouble(res[1] + ""));
                idx = (int) res[0];
            }
            idx++;
        }
        while (operationSubStack.size() > 0) {
            double second = operandSubStack.pop();
            double first = operandSubStack.pop();
            char op = operationSubStack.pop();
            applyOperation(operandSubStack, first, op, second);
        }
        double[] returnedValues = new double[2];
        returnedValues[0] = idx;
        returnedValues[1] = operandSubStack.peek();
        return returnedValues;
    }

    // Every push to operationStack this method called to check if top of stack has higher priority
    // than operation to be pushed, if it has higher priority the method applyOperation will invoke
    private void checkPriority(Stack<Double> operandStack, Stack<Character> operationStack, char operation, String eq, int idx) {
        while (getPriority(operationStack.peek()) > getPriority(operation)) {
            double second = operandStack.pop();
            double first = operandStack.pop();
            char op = operationStack.pop();
            applyOperation(operandStack, first, op, second);
        }
        operationStack.push(operation);
    }

    // apply an arithmetic operation and return it result
    private void applyOperation(Stack<Double> operandStack, double first, char operation, double second) {
        double result = 0;
        switch (operation) {
            case '+': {
                result = first + second;
                break;
            }
            case '-': {
                result = first - second;
                break;
            }
            case '/': {
                result = first / second;
                break;
            }
            case '*': {
                result = first * second;
                break;
            }
            case '^': {
                result = Math.pow(first, second);
                break;
            }
        }
        operandStack.push(result);
    }

    // return integer number represent the priority of operation
    private int getPriority(Character operation) {
        int priority = 0;
        switch (operation) {
            case '+':
            case '-': {
                priority = 0;
                break;
            }
            case '*':
            case '/': {
                priority = 1;
                break;
            }
            case '^': {
                priority = 2;
            }
        }
        return priority;
    }

    // get an operation from the equation
    private char getOperation(String eq, int idx) {
        return eq.charAt(idx);
    }

    // check if the character is an operation or not
    private boolean isOperator(char ch) {
        switch (ch) {
            case '+':
            case '-':
            case '*':
            case '/':
            case '%':
            case '^':
                return true;
            default:
                return false;
        }
    }

    // get numbers from equation
    private int getOperand(String eq, int idx) {
        StringBuilder stringBuilder = new StringBuilder();
        while (idx < eq.length() && Character.isDigit(eq.charAt(idx))) {
            stringBuilder.append(eq.charAt(idx));
            idx++;
        }
        return Integer.parseInt(stringBuilder.toString());
    }

    // looking for variables and ask user about it's values
    private String lookForVariablesAndGetValues(String eq) {
        Scanner scanner = new Scanner(System.in);
        StringBuilder newEquation = new StringBuilder();
        HashMap<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < eq.length(); i++) {
            if (Character.isAlphabetic(eq.charAt(i))) {
                if (map.containsKey(eq.charAt(i))) {
                    newEquation.append(map.get(eq.charAt(i)));
                } else {
                    System.out.print("Input the value of " + eq.charAt(i) + ": ");
                    int value = scanner.nextInt();
                    map.put(eq.charAt(i), value);
                    newEquation.append(value);
                }
            } else {
                newEquation.append(eq.charAt(i));
            }
        }
        return newEquation.toString();
    }

    // remove all spaces from equation to prepare it for calculation
    private String removeSpaces(String eq) {
        eq = eq.trim();
        StringBuilder newEq = new StringBuilder();
        for (int i = 0; i < eq.length(); i++) {
            if (eq.charAt(i) != ' ') {
                newEq.append(eq.charAt(i));
            }
        }
        return newEq.toString();
    }

    // to check if it's valid equation and parentheses are balanced
    private void checkParenthesesBalance(String eq) {
        boolean flag = true;
        Stack<Character> parentheses = new Stack<>();
        for (int i = 0; i < eq.length(); i++) {
            char current = eq.charAt(i);
            if (current == '(' || current == ')') {
                if (current == '(') parentheses.push(current);
                else if (current == ')' && parentheses.size() > 0) parentheses.pop();
                else {
                    flag = false;
                    break;
                }
            }
        }
        if (flag && parentheses.size() > 0) flag = !flag;
        if (!flag)
            throw new IllegalArgumentException("There's a problem with number of open-parentheses and close-parentheses, or it's not balanced.");
    }
}
